module gitlab.com/Vyunnikov_Viktor/go-micro-proj

go 1.13

require (
	github.com/gogo/protobuf v1.3.1 // indirect
	github.com/golang/protobuf v1.3.2
	github.com/google/jsonapi v0.0.0-20181016150055-d0428f63eb51
	github.com/gorilla/mux v1.7.3
	github.com/jackc/fake v0.0.0-20150926172116-812a484cc733 // indirect
	github.com/jackc/pgtype v1.0.3 // indirect
	github.com/jackc/pgx v3.6.0+incompatible
	github.com/jackc/pgx/v4 v4.1.2
	github.com/jmoiron/sqlx v1.2.0
	github.com/json-iterator/go v1.1.8 // indirect
	github.com/lucas-clemente/quic-go v0.13.1 // indirect
	github.com/micro/go-micro v1.16.0
	github.com/miekg/dns v1.1.22 // indirect
	github.com/nats-io/jwt v0.3.2 // indirect
	github.com/nats-io/nats.go v1.9.1 // indirect
	go.uber.org/atomic v1.5.1 // indirect
	go.uber.org/multierr v1.4.0 // indirect
	go.uber.org/zap v1.13.0 // indirect
	golang.org/x/crypto v0.0.0-20191119213627-4f8c1d86b1ba // indirect
	golang.org/x/net v0.0.0-20191119073136-fc4aabc6c914
	golang.org/x/sys v0.0.0-20191120155948-bd437916bb0e // indirect
	golang.org/x/tools v0.0.0-20191121040551-947d4aa89328 // indirect
	golang.org/x/xerrors v0.0.0-20191011141410-1b5146add898 // indirect
	google.golang.org/genproto v0.0.0-20191115221424-83cc0476cb11 // indirect
	google.golang.org/grpc v1.25.1 // indirect
)
