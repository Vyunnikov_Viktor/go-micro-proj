package main

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/micro/go-micro"
	pb "gitlab.com/Vyunnikov_Viktor/go-micro-proj/user-service/proto/user"
)

type UserClient struct {
	client pb.UserServiceClient
}

func (c *UserClient) GetAll() {
	fmt.Println("...:::GetAll:::...")
	response, err := c.client.GetAll(context.Background(), &pb.GetAllRequest{})
	answer, err := json.Marshal(response)
	fmt.Println("Response = ", string(answer))
	fmt.Println("Error =  ", err)
}

func (c *UserClient) Get(userId int32) {
	fmt.Println("...:::Get:::...")
	response, err := c.client.Get(context.Background(), &pb.GetRequest{Id: userId})

	fmt.Println("Response = ", response)
	fmt.Println("Error = ", err)
}

func (c *UserClient) Crete(user *pb.User) {
	fmt.Println("...:::Crete:::...")
	response, err := c.client.Create(context.Background(), &pb.CreateRequest{User: user})

	fmt.Println("Response = ", response)
	fmt.Println("Error = ", err)
}

func main() {

	service := micro.NewService(micro.Name("go-micro-proj.client.user"))
	service.Init()

	client := pb.NewUserServiceClient("go-micro-proj.service.user", service.Client())

	userClient := UserClient{client}

	//userClient.Crete(&pb.User{
	//	Id:       0,
	//	Username: "Username",
	//	FullName:     "FullName",
	//	Password: "Password",
	//})

	userClient.GetAll()
	//userClient.Get(1)
	//userClient.Get(10)

}
