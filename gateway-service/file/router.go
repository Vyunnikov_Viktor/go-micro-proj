package file

import (
	"net/http"
	"net/http/httputil"
	"net/url"
)

type RestController interface {
	Upload() http.HandlerFunc
	Load() http.HandlerFunc
}

type RestControllerImpl struct {
}

func (r *RestControllerImpl) Load() http.HandlerFunc {
	proxyUrl, _ := url.Parse("http://localhost:8081")

	return func(w http.ResponseWriter, req *http.Request) {

		proxy := httputil.NewSingleHostReverseProxy(proxyUrl)

		req.URL.Host = proxyUrl.Host
		req.URL.Scheme = proxyUrl.Scheme
		req.Header.Set("X-Forwarded-Host", req.Header.Get("Host"))
		req.Host = proxyUrl.Host

		proxy.ServeHTTP(w, req)
	}
}

func (r *RestControllerImpl) Upload() http.HandlerFunc {
	proxyUrl, _ := url.Parse("http://localhost:8081")

	return func(w http.ResponseWriter, req *http.Request) {

		proxy := httputil.NewSingleHostReverseProxy(proxyUrl)

		req.URL.Host = proxyUrl.Host
		req.URL.Scheme = proxyUrl.Scheme
		req.Header.Set("X-Forwarded-Host", req.Header.Get("Host"))
		req.Host = proxyUrl.Host

		proxy.ServeHTTP(w, req)
	}
}
