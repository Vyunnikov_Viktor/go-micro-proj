package main

import (
	"github.com/gorilla/mux"
	"github.com/micro/go-micro"
	"gitlab.com/Vyunnikov_Viktor/go-micro-proj/gateway-service/file"
	_ "gitlab.com/Vyunnikov_Viktor/go-micro-proj/gateway-service/file"
	"gitlab.com/Vyunnikov_Viktor/go-micro-proj/gateway-service/user"
	userpb "gitlab.com/Vyunnikov_Viktor/go-micro-proj/user-service/proto/user"
)

func main() {
	//registry:=mdns.NewRegistry(mdns.Domain("go-micro-proj.service.user"))
	//registry.GetService()
	service := micro.NewService(micro.Name("go-micro-proj.service.gateway"))

	service.Init()

	userClient := userpb.NewUserServiceClient("go-micro-proj.service.user", service.Client())

	userService := &user.ServiceGPRCImpl{
		UserClient: userClient,
	}

	controllerV1 := user.RestControllerV1Impl{
		UserService: userService,
	}

	controllerV2 := user.RestControllerV2Impl{
		UserService: userService,
	}

	server := Server{
		mux.NewRouter(),
		&controllerV1,
		&controllerV2,
		&file.RestControllerImpl{},
	}

	server.init()
}
