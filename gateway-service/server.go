package main

import (
	"github.com/gorilla/mux"
	"gitlab.com/Vyunnikov_Viktor/go-micro-proj/gateway-service/file"
	"gitlab.com/Vyunnikov_Viktor/go-micro-proj/gateway-service/user"
	"net/http"
)

// vars := mux.Vars(r)
//        fmt.Println(vars)
//        lol := r.URL.Query()
//        v, found := lol["lol"]
//        fmt.Println(v)
//        fmt.Println(found)

type Server struct {
	route        *mux.Router
	userRouterV1 user.RestController
	userRouterV2 user.RestController
	fileRouter   file.RestController
}

func (s *Server) init() {

	apiV1Route := s.route.PathPrefix("/proj/api/v1/").Subrouter()

	apiV1Route.HandleFunc("/users", s.userRouterV1.GetUsers()).
		Methods("GET")
	apiV1Route.HandleFunc("/users/{id:[0-9]+}", s.userRouterV1.GetUsersById()).
		Methods("GET")
	apiV1Route.HandleFunc("/users", s.userRouterV1.PostUsers()).
		Methods("POST")

	apiV1Route.HandleFunc("/files/{id:[0-9]+}/load", s.fileRouter.Load()).
		Methods("GET")
	apiV1Route.HandleFunc("/files/upload", s.fileRouter.Upload()).
		Methods("POST")

	apiV2Route := s.route.PathPrefix("/proj/api/v2/").Subrouter()
	apiV2Route.HandleFunc("/users", s.userRouterV2.GetUsers()).
		Methods("GET")
	apiV2Route.HandleFunc("/users/{id:[0-9]+}", s.userRouterV2.GetUsersById()).
		Methods("GET")
	apiV2Route.HandleFunc("/users", s.userRouterV2.PostUsers()).
		Methods("POST")

	if err := http.ListenAndServe(":8080", s.route); err != nil {
		panic(err)
	}
}
