package user

import (
	"context"
	"github.com/google/jsonapi"
	"github.com/gorilla/mux"
	"gitlab.com/Vyunnikov_Viktor/go-micro-proj/gateway-service/parseutils"
	"net/http"
	"strconv"
)

type RestControllerV2Impl struct {
	UserService Service
}

func (s *RestControllerV2Impl) GetUsersById() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		vars := mux.Vars(r)
		userId, _ := strconv.Atoi(vars["id"])

		protobufUser, err := s.UserService.GetById(context.Background(), userId)

		if err != nil {
			parseutils.RespondWithError(w, err)
			return
		}

		parseutils.RespondWithJsonApi(w, FromProtoBuf(protobufUser), 200)
	}
}

func (s *RestControllerV2Impl) PostUsers() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		modelUser := &User{}

		if err := jsonapi.UnmarshalPayload(r.Body, modelUser); err != nil {
			parseutils.RespondWithError(w, err)
			return
		}

		protoUser, err := s.UserService.Create(context.Background(), ToProtoBuf(modelUser))

		if err != nil {
			parseutils.RespondWithError(w, err)
			return
		}

		parseutils.RespondWithJsonApi(w, FromProtoBuf(protoUser), 201)
	}
}

func (s *RestControllerV2Impl) GetUsers() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		protobufUsers, err := s.UserService.GetAll(context.Background())

		if err != nil {
			parseutils.RespondWithError(w, err)
			return
		}

		parseutils.RespondWithJsonApi(w, FromProtoBufArray(protobufUsers), 200)
	}
}
