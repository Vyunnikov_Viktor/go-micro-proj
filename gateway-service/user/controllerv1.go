package user

import (
	"context"
	"encoding/json"
	"github.com/gorilla/mux"
	"gitlab.com/Vyunnikov_Viktor/go-micro-proj/gateway-service/parseutils"
	pb "gitlab.com/Vyunnikov_Viktor/go-micro-proj/user-service/proto/user"
	"net/http"
	"strconv"
)

type RestController interface {
	GetUsers() http.HandlerFunc
	GetUsersById() http.HandlerFunc
	PostUsers() http.HandlerFunc
}

type RestControllerV1Impl struct {
	UserService Service
}

func (s *RestControllerV1Impl) GetUsers() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		protobufUsers, err := s.UserService.GetAll(context.Background())

		if err != nil {
			parseutils.RespondWithError(w, err)
			return
		}

		parseutils.RespondWithJson(w, protobufUsers, 200)
	}
}

func (s *RestControllerV1Impl) GetUsersById() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		vars := mux.Vars(r)
		userId, _ := strconv.Atoi(vars["id"])

		user, err := s.UserService.GetById(context.Background(), userId)

		if err != nil {
			parseutils.RespondWithError(w, err)
			return
		}

		parseutils.RespondWithJson(w, user, 200)
	}
}

func (s *RestControllerV1Impl) PostUsers() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		user := &pb.User{}
		err := json.NewDecoder(r.Body).Decode(user)

		if err != nil {
			parseutils.RespondWithError(w, err)
			return
		}

		user, err = s.UserService.Create(context.Background(), user)

		if err != nil {
			parseutils.RespondWithError(w, err)
			return
		}

		parseutils.RespondWithJson(w, user, 201)
	}
}
