package user

import (
	"context"
	"gitlab.com/Vyunnikov_Viktor/go-micro-proj/gateway-service/parseutils"
	pb "gitlab.com/Vyunnikov_Viktor/go-micro-proj/user-service/proto/user"
)

type Service interface {
	GetAll(ctx context.Context) ([]*pb.User, error)
	GetById(ctx context.Context, userId int) (*pb.User, error)
	Create(ctx context.Context, user *pb.User) (*pb.User, error)
}

type ServiceGPRCImpl struct {
	UserClient pb.UserServiceClient
}

func (s *ServiceGPRCImpl) GetAll(ctx context.Context) ([]*pb.User, error) {
	getAllResponse, requestError := s.UserClient.GetAll(context.Background(), &pb.GetAllRequest{})

	if err := s.ParseError(requestError, func() *pb.Error { return getAllResponse.Err }); err != nil {
		return nil, err
	}

	users := make([]*pb.User, 0)
	if getAllResponse.Users != nil {
		users = getAllResponse.Users
	}
	return users, nil
}

func (s *ServiceGPRCImpl) GetById(ctx context.Context, userId int) (*pb.User, error) {

	getResponse, requestError := s.UserClient.Get(
		context.Background(),
		&pb.GetRequest{
			Id: int32(userId),
		},
	)

	if err := s.ParseError(requestError, func() *pb.Error { return getResponse.Err }); err != nil {
		return nil, err
	}

	return getResponse.User, nil
}

func (s *ServiceGPRCImpl) Create(ctx context.Context, user *pb.User) (*pb.User, error) {
	createResponse, requestError := s.UserClient.Create(context.Background(), &pb.CreateRequest{User: user})

	if err := s.ParseError(requestError, func() *pb.Error { return createResponse.Err }); err != nil {
		return nil, err
	}

	return createResponse.User, nil
}

func (s *ServiceGPRCImpl) ParseError(err error, getGRPCError func() *pb.Error) error {
	if err != nil {
		return &parseutils.ApiError{
			Code:        500,
			Name:        "InternalServerError",
			Description: err.Error(),
		}
	}

	if userError := getGRPCError(); userError != nil {
		return &parseutils.ApiError{
			Code:        int(userError.Code),
			Name:        userError.Name,
			Description: userError.Description,
		}
	}

	return nil
}
