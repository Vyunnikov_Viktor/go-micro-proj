package user

import (
	pb "gitlab.com/Vyunnikov_Viktor/go-micro-proj/user-service/proto/user"
)

type User struct {
	Id       int32   `json:"id,omitempty" jsonapi:"primary,users"`
	Username string  `json:"username,omitempty" jsonapi:"attr,user_name"`
	FullName string  `json:"name,omitempty" jsonapi:"attr,full_name"`
	Password string  `json:"password,omitempty" jsonapi:"attr,password"`
	Roles    []*Role `json:"roles,omitempty" jsonapi:"relation,roles"`
}

//func (u User) JSONAPIRelationshipLinks(relation string) *jsonapi.Links {
//    if relation == "roles" {
//        return &jsonapi.Links{
//            "related": fmt.Sprintf("https://example.com/posts/%d/comments", u.Id),
//        }
//    }
//    return nil
//}

type Role struct {
	Id       int32  `json:"id,omitempty" jsonapi:"primary,roles"`
	RoleName string `json:"username,omitempty" jsonapi:"attr,role_name"`
}

func FromProtoBufArray(users []*pb.User) []*User {
	res := make([]*User, len(users))
	for i, user := range users {
		res[i] = &User{
			Id:       user.Id,
			Username: user.Username,
			FullName: user.FullName,
			Password: user.Password,
		}
		res[i].Roles = make([]*Role, len(user.Roles))
		for j, role := range user.Roles {
			res[i].Roles[j] = &Role{
				Id:       role.Id,
				RoleName: role.RoleName,
			}
		}
	}

	return res
}

func FromProtoBuf(user *pb.User) *User {

	res := &User{
		Id:       user.Id,
		Username: user.Username,
		FullName: user.FullName,
		Password: user.Password,
	}
	res.Roles = make([]*Role, len(user.Roles))
	for j, role := range user.Roles {
		res.Roles[j] = &Role{
			Id:       role.Id,
			RoleName: role.RoleName,
		}
	}

	return res
}

func ToProtoBuf(user *User) *pb.User {

	res := &pb.User{
		Id:       user.Id,
		Username: user.Username,
		FullName: user.FullName,
		Password: user.Password,
	}
	res.Roles = make([]*pb.Role, len(user.Roles))
	for j, role := range user.Roles {
		res.Roles[j] = &pb.Role{
			Id:       role.Id,
			RoleName: role.RoleName,
		}
	}

	return res
}
