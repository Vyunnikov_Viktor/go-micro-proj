package main

import (
	_ "github.com/jackc/pgx/stdlib"
	"github.com/jmoiron/sqlx"
	_ "github.com/jmoiron/sqlx"
)

func CreateConnection() (*sqlx.DB, error) {
	db, err := sqlx.Connect("pgx", "postgres://postgres:postgres@localhost:5431/postgres")

	return db, err
}
