package main

import (
	"io/ioutil"
	"os"
)

type FileService interface {
	Create(fileBytes []byte, fileName string, size int32, dirPath string) (*File, error)
	GetById(fileId int) (*File, error)
}

type FileServiceImpl struct {
	repo FileRepository
}

func (s *FileServiceImpl) GetById(fileId int) (*File, error) {
	return s.repo.GetById(fileId)
}

func (s *FileServiceImpl) Create(fileBytes []byte, fileName string, size int32, dirPath string) (*File, error) {

	tempFile, err := ioutil.TempFile(dirPath, "upload-*.png")
	if err != nil {
		return nil, err
	}
	defer tempFile.Close()

	if _, err := tempFile.Write(fileBytes); err != nil {
		return nil, err
	}

	fileModel, err := s.repo.Create(&File{
		VirtualPath: fileName,
		StoragePath: tempFile.Name(),
		Size:        size,
	})

	if err != nil {
		_ = os.Remove(tempFile.Name())
		return nil, err
	}

	return fileModel, nil
}
