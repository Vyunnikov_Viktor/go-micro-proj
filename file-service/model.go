package main

type File struct {
	Id          int64  `json:"id,omitempty" db:"id"`
	VirtualPath string `json:"virtual_path,omitempty" db:"virtual_path"`
	StoragePath string `json:"storage_path,omitempty" db:"storage_path"`
	Size        int32  `json:"size,omitempty" db:"size"`
}
