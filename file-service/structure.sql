create table file (
  id serial primary key ,
  virtual_path varchar unique,
  storage_path varchar unique,
  size int
);