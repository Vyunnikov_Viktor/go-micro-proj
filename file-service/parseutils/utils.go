package parseutils

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type ApiError struct {
	Code        int
	Name        string
	Description string
}

func (a ApiError) Error() string {
	return fmt.Sprintf("Error code=%v name=%v description=%v", a.Code, a.Name, a.Description)
}

func RespondWithJson(w http.ResponseWriter, res interface{}, code int) {
	w.Header().Add("Content-Type", "application/json")
	data, err := json.Marshal(res)

	if err != nil {
		RespondWithError(w, err)
		return
	}

	w.WriteHeader(code)
	w.Write(data)
}

func RespondWithError(w http.ResponseWriter, err error) {
	w.Header().Add("Content-Type", "application/json")

	var name, description string
	var code int

	if v, ok := err.(*ApiError); ok {
		name = v.Name
		description = v.Description
		code = v.Code
	} else {
		name = "InternalServerError"
		description = err.Error()
		code = 500
	}

	w.WriteHeader(code)

	res := map[string]interface{}{
		"error_code":  code,
		"name":        name,
		"description": description}

	json.NewEncoder(w).Encode(res)
}
