package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/Vyunnikov_Viktor/go-micro-proj/file-service/parseutils"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
)

type RestController interface {
	Upload() http.HandlerFunc
	Load() http.HandlerFunc
}

type RestControllerImpl struct {
	service FileService
}

func (r *RestControllerImpl) Load() http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {

		fileId, _ := strconv.Atoi(mux.Vars(req)["id"])

		fileModel, err := r.service.GetById(fileId)

		if err != nil {
			parseutils.RespondWithError(w, err)
			return
		}

		split := strings.Split(fileModel.VirtualPath, "/")
		fileName := split[len(split)-1]

		w.Header().Set("Content-Disposition", fmt.Sprintf("attachment; filename=%v", fileName))
		w.Header().Set("Content-Type", req.Header.Get("Content-Type"))

		file, err := os.Open(fmt.Sprintf(fileModel.StoragePath))

		if err != nil {
			parseutils.RespondWithError(w, err)
			return
		}
		defer file.Close()

		w.Header().Set("Content-Length", strconv.Itoa(int(fileModel.Size)))

		io.Copy(w, file)
	}
}

func (r *RestControllerImpl) Upload() http.HandlerFunc {
	dirPath := "upload_folder"

	if err := os.MkdirAll(dirPath, os.ModePerm); err != nil {
		log.Fatal(err)
	}
	return func(w http.ResponseWriter, req *http.Request) {

		virtualPath, ok := req.URL.Query()["name"]

		if !ok {
			parseutils.RespondWithError(w, parseutils.ApiError{
				Code:        400,
				Name:        "BadRequest",
				Description: "There is no file name",
			})
			return
		}

		if err := req.ParseMultipartForm(10 << 20); err != nil {
			parseutils.RespondWithError(w, err)
			return
		}

		file, handler, err := req.FormFile("myFile")
		if err != nil {
			parseutils.RespondWithError(w, err)
			return
		}
		defer file.Close()

		fileBytes, err := ioutil.ReadAll(file)
		if err != nil {
			parseutils.RespondWithError(w, err)
			return
		}
		fileModel, err := r.service.Create(fileBytes, virtualPath[0], int32(handler.Size), dirPath)

		if err != nil {
			parseutils.RespondWithError(w, err)
			return
		}

		parseutils.RespondWithJson(w, fileModel, 201)
	}
}
