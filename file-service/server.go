package main

import (
	"github.com/gorilla/mux"
	"net/http"
)

type Server struct {
	route      *mux.Router
	fileRouter RestController
}

func (s *Server) init() {

	apiV1Route := s.route.PathPrefix("/proj/api/v1/").Subrouter()

	apiV1Route.HandleFunc("/files/upload", s.fileRouter.Upload()).
		Methods("POST")
	apiV1Route.HandleFunc("/files/{id:[0-9]+}/load", s.fileRouter.Load()).
		Methods("GET")

	if err := http.ListenAndServe(":8081", s.route); err != nil {
		panic(err)
	}
}
