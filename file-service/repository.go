package main

import (
	"github.com/jmoiron/sqlx"
)

type FileRepository interface {
	Create(file *File) (*File, error)
	GetById(fileId int) (*File, error)
}

type FileRepositorySqlxImpl struct {
	db *sqlx.DB
}

func (r *FileRepositorySqlxImpl) GetById(fileId int) (*File, error) {
	file := &File{}
	err := r.db.Get(file, "SELECT id, virtual_path, storage_path, size from file where id = $1", fileId)

	if err != nil {
		return nil, err
	}

	return file, nil
}

func (r *FileRepositorySqlxImpl) Create(file *File) (*File, error) {
	err := r.db.QueryRow(
		"INSERT into file (virtual_path, storage_path, size) VALUES ($1,$2,$3) returning id",
		&file.VirtualPath, &file.StoragePath, &file.Size,
	).Scan(&file.Id)

	if err != nil {
		return nil, err
	}

	return file, nil
}
