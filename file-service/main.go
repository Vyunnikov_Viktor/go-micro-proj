package main

import (
	"github.com/gorilla/mux"
	"github.com/micro/go-micro"
	"log"
)

func main() {
	service := micro.NewService(micro.Name("go-micro-proj.service.file"))
	service.Init()

	db, err := CreateConnection()

	if err != nil {
		log.Fatal(err)
	}

	fileRepo := &FileRepositorySqlxImpl{db}
	fileService := &FileServiceImpl{fileRepo}
	server := Server{
		mux.NewRouter(),
		&RestControllerImpl{fileService},
	}

	server.init()
}
