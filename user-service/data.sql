insert into users (username, full_name, password)
values ('admin', 'admin name', 'qwerty'),
       ('user', 'username', 'lol');

insert into role (role_name)
values ('ADMIN'),
       ('USER');

insert into user_role (user_id, role_id)
VALUES (1, 1),
       (1, 2),
       (2, 1);

select user_id, username, full_name, password, role_id, role_name
from users
         join user_role ur
              on users.id = ur.user_id
         join role r on ur.role_id = r.id
order by user_id;

select user_id, username, full_name, password, role_id, role_name
from users
         join user_role ur
              on users.id = ur.user_id
         join role r on ur.role_id = r.id
where users.id = 1;


