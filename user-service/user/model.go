package user

import (
	pb "gitlab.com/Vyunnikov_Viktor/go-micro-proj/user-service/proto/user"
	"gitlab.com/Vyunnikov_Viktor/go-micro-proj/user-service/role"
)

type User struct {
	Id       *int
	Username *string
	FullName *string
	Password *string
	Roles    []*role.Role
}

func ToProtoBufArray(users []*User) []*pb.User {
	res := make([]*pb.User, len(users))
	for i, user := range users {
		res[i] = &pb.User{
			Id:       int32(*user.Id),
			Username: *user.Username,
			FullName: *user.FullName,
			Password: *user.Password,
		}
		res[i].Roles = make([]*pb.Role, len(user.Roles))
		for j, userRole := range user.Roles {
			res[i].Roles[j] = &pb.Role{
				Id:       int32(*userRole.Id),
				RoleName: *userRole.RoleName,
			}
		}
	}

	return res
}

func ToProtoBuf(user *User) *pb.User {
	res := &pb.User{
		Id:       int32(*user.Id),
		Username: *user.Username,
		FullName: *user.FullName,
		Password: *user.Password,
	}
	res.Roles = make([]*pb.Role, len(user.Roles))
	for j, userRole := range user.Roles {
		res.Roles[j] = &pb.Role{
			Id:       int32(*userRole.Id),
			RoleName: *userRole.RoleName,
		}
	}

	return res
}

func FromProtoBuf(user *pb.User) *User {
	var userId = int(user.Id)
	res := &User{
		Id:       &userId,
		Username: &user.Username,
		FullName: &user.FullName,
		Password: &user.Password,
	}
	res.Roles = make([]*role.Role, len(user.Roles))
	for j, userRole := range user.Roles {
		var roleId = int(userRole.Id)
		res.Roles[j] = &role.Role{
			Id:       &roleId,
			RoleName: &userRole.RoleName,
		}
	}

	return res
}
