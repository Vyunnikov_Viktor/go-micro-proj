package user

import (
	"context"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/Vyunnikov_Viktor/go-micro-proj/user-service/role"
)

type Repository interface {
	Get(ctx context.Context, userId int) (*User, error)
	GetAll(ctx context.Context) ([]*User, error)
	Create(ctx context.Context, user *User) (*User, error)
	CreateTransaction(context.Context) (pgx.Tx, error)
}

type RepositoryPgxImpl struct {
	Conn *pgxpool.Pool
}

func (r *RepositoryPgxImpl) CreateTransaction(ctx context.Context) (pgx.Tx, error) {
	return r.Conn.Begin(ctx)
}

var selectUserById = "select users.id as user_id, username, full_name, password, role_id, role_name" +
	" from users" +
	" left join user_role ur" +
	" on users.id = ur.user_id" +
	" left join role r on ur.role_id = r.id" +
	" where users.id = $1;"

func (r *RepositoryPgxImpl) Get(ctx context.Context, userId int) (*User, error) {

	var rows pgx.Rows
	var err error

	if tx, ok := ctx.Value("tx").(pgx.Tx); ok {
		rows, err = tx.Query(ctx, selectUserById, userId)
	} else {
		rows, err = r.Conn.Query(ctx, selectUserById, userId)
	}

	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var user *User

	for rows.Next() {
		if user == nil {
			user = &User{}
		}

		newRole := &role.Role{}

		err = rows.Scan(&user.Id, &user.Username, &user.FullName, &user.Password, &newRole.Id, &newRole.RoleName)
		if err != nil {
			return nil, err
		}
		if newRole.Id != nil {
			user.Roles = append(user.Roles, newRole)
		}
	}

	err = rows.Err()
	if err != nil {
		return nil, err
	}

	return user, nil
}

var selectAll = "select users.id as user_id, username, full_name, password, role_id, role_name" +
	" from users" +
	" left join user_role ur" +
	" on users.id = ur.user_id" +
	" left join role r on ur.role_id = r.id" +
	" order by users.id;"

func (r *RepositoryPgxImpl) GetAll(ctx context.Context) ([]*User, error) {

	var rows pgx.Rows
	var err error

	if tx, ok := ctx.Value("tx").(pgx.Tx); ok {
		rows, err = tx.Query(ctx, selectAll)
	} else {
		rows, err = r.Conn.Query(ctx, selectAll)
	}

	if err != nil {
		return nil, err
	}
	defer rows.Close()

	users := make([]*User, 0)

	var lastUser *User = nil

	for rows.Next() {
		user := &User{}
		newRole := &role.Role{}
		err := rows.Scan(&user.Id, &user.Username, &user.FullName, &user.Password, &newRole.Id, &newRole.RoleName)
		if err != nil {
			return nil, err
		}

		if lastUser == nil {
			lastUser = user
		} else if *lastUser.Id != *user.Id {
			users = append(users, lastUser)
			lastUser = user
		}

		if newRole.Id != nil {
			lastUser.Roles = append(lastUser.Roles, newRole)
		}
	}

	if lastUser != nil {
		users = append(users, lastUser)
	}

	err = rows.Err()
	if err != nil {
		return nil, err
	}

	return users, nil
}

var insertUser = "insert into users(username, full_name, password) values($1,$2,$3) returning id"

func (r *RepositoryPgxImpl) Create(ctx context.Context, user *User) (*User, error) {
	var err error
	if tx, ok := ctx.Value("tx").(pgx.Tx); ok {
		err = tx.QueryRow(ctx, insertUser,
			user.Username, user.FullName, user.Password).Scan(&user.Id)
	} else {
		err = r.Conn.QueryRow(context.Background(),
			insertUser,
			user.Username, user.FullName, user.Password).Scan(&user.Id)
	}

	if err != nil {
		return nil, err
	}

	return user, nil
}
