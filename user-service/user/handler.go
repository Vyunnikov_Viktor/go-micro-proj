package user

import (
	"context"
	"fmt"
	pb "gitlab.com/Vyunnikov_Viktor/go-micro-proj/user-service/proto/user"
	"gitlab.com/Vyunnikov_Viktor/go-micro-proj/user-service/userrole"
)

type ServiceHandlerImpl struct {
	UserRepo     Repository
	UserRoleRepo userrole.Repository
}

func (s *ServiceHandlerImpl) Get(ctx context.Context, req *pb.GetRequest, res *pb.GetResponse) (returnErr error) {
	returnErr = nil

	user, err := s.UserRepo.Get(ctx, int(req.Id))

	if err != nil {
		//if err == pgx.ErrNoRows {
		RespondWithError(&res.Err, 500, "InternalServerError", err.Error())
		//}
	}

	if user == nil {
		RespondWithError(&res.Err, 404, "User not found", "User not found")
		return
	}

	//switch err {
	//case nil:
	//case pgx.ErrNoRows:
	//    RespondWithError(&res.Err, 500, "InternalServerError", err.Error())
	//    return
	//default:
	//    RespondWithError(&res.Err, 500, "InternalServerError", err.Error())
	//    return
	//}

	res.User = ToProtoBuf(user)
	fmt.Println(fmt.Sprintf("Get req=%v res=%v", req, res))
	return
}

func (s *ServiceHandlerImpl) GetAll(ctx context.Context, req *pb.GetAllRequest, res *pb.GetAllResponse) (returnErr error) {
	returnErr = nil

	users, err := s.UserRepo.GetAll(ctx)

	switch err {
	case nil:
	default:
		RespondWithError(&res.Err, 500, "InternalServerError", err.Error())
		return
	}
	res.Users = ToProtoBufArray(users)
	fmt.Println(fmt.Sprintf("GetAll req=%v res=%v", req, res))
	return
}

func (s *ServiceHandlerImpl) Create(ctx context.Context, req *pb.CreateRequest, res *pb.CreateResponse) (returnErr error) {

	tx, err := s.UserRepo.CreateTransaction(ctx)

	if err != nil {
		RespondWithError(&res.Err, 500, "InternalServerError", err.Error())
		return
	}
	defer tx.Rollback(ctx)

	newCtx := context.WithValue(ctx, "tx", tx)
	modelUser := FromProtoBuf(req.User)

	user, err := s.UserRepo.Create(newCtx, modelUser)
	if err != nil {
		RespondWithError(&res.Err, 500, "InternalServerError", err.Error())
		return
	}
	err = s.UserRoleRepo.PutRoles(newCtx, *user.Id, modelUser.Roles)
	if err != nil {
		RespondWithError(&res.Err, 500, "InternalServerError", err.Error())
		return
	}

	finalUser, err := s.UserRepo.Get(newCtx, *user.Id)

	if err != nil {
		RespondWithError(&res.Err, 500, "InternalServerError", err.Error())
		return
	}

	if err = tx.Commit(ctx); err != nil {
		RespondWithError(&res.Err, 500, "InternalServerError", err.Error())
		return
	}
	res.User = ToProtoBuf(finalUser)
	fmt.Println(fmt.Sprintf("Create req=%v res=%v", req, res))
	return
}

func RespondWithError(res **pb.Error, code int, name, description string) {
	*res = &pb.Error{
		Code:        int32(code),
		Name:        name,
		Description: description,
	}
}
