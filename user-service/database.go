package main

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v4/pgxpool"
	"os"
	"strconv"
	"time"
)

const (
	defaultHost     = "0.0.0.0"
	defaultPort     = "5432"
	defaultDBName   = "my_nip"
	defaultUser     = "my_nip"
	defaultPassword = "my_nip"
)

func CreateConnection() (*pgxpool.Pool, error) {

	var port, host, user, DBName, password string
	var present bool
	// Get database details from environment variables

	if port, present = os.LookupEnv("DB_PORT"); !present {
		fmt.Println("Using default port", defaultPort)
		port = defaultPort
	}

	if host, present = os.LookupEnv("DB_HOST"); !present {
		fmt.Println("Using default host", defaultHost)
		host = defaultHost
	}

	if user, present = os.LookupEnv("DB_USER"); !present {
		fmt.Println("Using default user", defaultUser)
		user = defaultUser
	}

	if DBName, present = os.LookupEnv("DB_NAME"); !present {
		fmt.Println("Using default DBName", defaultDBName)
		DBName = defaultDBName
	}

	if password, present = os.LookupEnv("DB_PASSWORD"); !present {
		fmt.Println("Using default password", defaultPassword)
		password = defaultPassword
	}

	conf, _ := pgxpool.ParseConfig("")
	conf.ConnConfig.Host = host
	conf.ConnConfig.Database = DBName
	conf.ConnConfig.User = user
	conf.ConnConfig.Password = password

	if host != "database" {
		intPort, err := strconv.Atoi(port)

		if err != nil {
			return nil, err
		}
		conf.ConnConfig.Port = uint16(intPort)
	}

	fmt.Println("host", host, "port", port, DBName, "user", user, "password", password)

	for reTries := 5; ; reTries-- {

		fmt.Println("Try to connect to database", reTries)

		pool, err := pgxpool.ConnectConfig(
			context.Background(),
			conf,
		)

		if err == nil {
			fmt.Println("Database connected")
			return pool, nil
		} else if reTries < 0 {
			return nil, err
		}
		fmt.Println(err)
		time.Sleep(4000 * time.Millisecond)
	}
}
