~~~
User {
    id 
    username
    fullname
}

Role {
    id
    name
}

User_Role {
    user_id
    role_id
}
~~~
### USERS

```GET /users```

~~~
HTTP/1.1 200 OK
Content-Type: application/vnd.api+json
{
    "data": [{
        "type": "users",
        "id": "1",
        "attributes": {
            "username": "USERNAME",
            "fullname": "FULLNAME",
        },
        "relationships": {
            "roles": [
                {"data": {"id": "1", "type": "roles"}}
            ]
        }],
    "included": [
        {
            "type": "roles",
            "id": "1",
            "attributes": {
                "name": "ADMIN"
            }
        }
}
~~~
```GET /users/1```

~~~
HTTP/1.1 200 OK
Content-Type: application/vnd.api+json
{
    "data": {
        "type": "users",
        "id": "1",
        "attributes": {
            "username": "USERNAME",
            "fullname": "FULLNAME",
        },
        "relationships": {
            "roles": [
                {"data": {"id": "1", "type": "roles"}}
            ]
        },
    "included": [
        {
            "type": "roles",
            "id": "1",
            "attributes": {
                "name": "ADMIN"
            }
        }
}
~~~

### ROLES
```GET /roles```

~~~
HTTP/1.1 200 OK
Content-Type: application/vnd.api+json
{
    "data": [{
        "type": "roles",
        "id": "1",
        "attributes": {
            "name": "USERNAME"
        },
        "relationships": {
            "roles": [
                {"data": {"id": "1", "type": "roles"}}
            ]
        }],
    "included": [
        {
            "type": "roles",
            "id": "1",
            "attributes": {
                "name": "ADMIN"
            }
        }
}
~~~