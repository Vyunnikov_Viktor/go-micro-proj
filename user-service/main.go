package main

import (
	"github.com/micro/go-micro"
	pb "gitlab.com/Vyunnikov_Viktor/go-micro-proj/user-service/proto/user"
	"gitlab.com/Vyunnikov_Viktor/go-micro-proj/user-service/user"
	"gitlab.com/Vyunnikov_Viktor/go-micro-proj/user-service/userrole"
	"log"
)

func main() {
	srv := micro.NewService(micro.Name("go-micro-proj.service.user"))

	connection, err := CreateConnection()

	if err != nil {
		log.Fatal(err)
	}

	//if err := connection.Ping(context.Background()); err != nil {
	//	log.Fatal(err)
	//}

	defer connection.Close()

	userRepository := &user.RepositoryPgxImpl{Conn: connection}
	userRoleRepository := &userrole.RepositoryPgxImpl{Conn: connection}

	pb.RegisterUserServiceHandler(srv.Server(), &user.ServiceHandlerImpl{UserRepo: userRepository, UserRoleRepo: userRoleRepository})

	if err := srv.Run(); err != nil {
		log.Fatal(err)
	}
}
