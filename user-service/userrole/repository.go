package userrole

import (
	"context"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/Vyunnikov_Viktor/go-micro-proj/user-service/role"
)

type Repository interface {
	Create(context.Context, *UserRole) error
	Delete(context.Context, *UserRole) error

	PutRoles(ctx context.Context, userId int, roleIds []*role.Role) error

	CreateTransaction(ctx context.Context) (pgx.Tx, error)
}

type RepositoryPgxImpl struct {
	Conn *pgxpool.Pool
}

var deleteRoles = "delete from user_role where user_id = $1"

func (r *RepositoryPgxImpl) PutRoles(ctx context.Context, userId int, roles []*role.Role) error {
	var tx pgx.Tx
	var err error
	var needCommit = false
	curCtx := ctx

	tx, ok := ctx.Value("tx").(pgx.Tx)

	if !ok {
		tx, err = r.CreateTransaction(ctx)
		if err != nil {
			return err
		}

		defer tx.Rollback(ctx)

		curCtx = context.WithValue(curCtx, "tx", tx)
		needCommit = true
	}

	_, err = tx.Exec(ctx, deleteRoles, userId)

	if err != nil {
		return err
	}

	for _, userRole := range roles {
		if err := r.Create(curCtx, &UserRole{UserId: &userId, RoleId: userRole.Id}); err != nil {
			return err
		}
	}

	if needCommit {
		if err := tx.Commit(curCtx); err != nil {
			return err
		}
	}

	return nil
}

var insertUserRole = "insert into user_role(user_id, role_id) values($1,$2)"

func (r *RepositoryPgxImpl) Create(ctx context.Context, useRole *UserRole) error {
	var err error
	if tx, ok := ctx.Value("tx").(pgx.Tx); ok {
		_, err = tx.Exec(ctx, insertUserRole, useRole.UserId, useRole.RoleId)
	} else {
		_, err = r.Conn.Exec(ctx, insertUserRole, useRole.UserId, useRole.RoleId)
	}

	if err != nil {
		return err
	}

	return nil
}

func (r *RepositoryPgxImpl) Delete(context.Context, *UserRole) error {
	panic("implement me")
}

func (r *RepositoryPgxImpl) CreateTransaction(ctx context.Context) (pgx.Tx, error) {
	return r.Conn.Begin(ctx)
}
