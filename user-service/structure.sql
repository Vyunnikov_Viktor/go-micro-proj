create table users (
    id serial primary key ,
    username varchar unique,
    full_name varchar,
    password varchar
);

create table role (
    id serial primary key ,
    role_name varchar
);

create table user_role (
    user_id int REFERENCES users(id) ON DELETE CASCADE,
    role_id int REFERENCES role(id) ON DELETE CASCADE,
    PRIMARY KEY (user_id,role_id)
);